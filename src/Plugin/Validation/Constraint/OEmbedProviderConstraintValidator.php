<?php

/**
 * @file
 * Contains \Drupal\media_entity_oembed\Plugin\Validation\Constraint\OEmbedProviderConstraintValidator.
 */

namespace Drupal\media_entity_oembed\Plugin\Validation\Constraint;

use Drupal\Core\Site\Settings;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the OEmbedProvider constraint.
 */
class OEmbedProviderConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    if (!isset($value) || empty($constraint->providers)) {
      return;
    }

    if (!$value->get($constraint->source_field)->isEmpty()) {
      $item = $value->get($constraint->source_field)[0];

      // @todo Add default embera config from settings.
      // @todo Move this code into a service since it's duplicated in the
      // OEmbedFormatter class.
      $config = array();
      if (!empty($constraint->negate)) {
        $config['deny'] = $constraint->providers;
      }
      else {
        $config['allow'] = $constraint->providers;
      }

      // Use Drupal-Settings for HTTP-proxy-Server (see settings.php).
      if ($proxy_server = Settings::get('proxy_server')) {
        $proxy = array(
          CURLOPT_PROXY => $proxy_server,
          CURLOPT_PROXYPORT => Settings::get('proxy_port', 8080),
        );

        if ($proxy_username = Settings::get('proxy_username')) {
          $proxy[CURLOPT_PROXYUSERPW] = $proxy_username . ':';
        }
        if ($proxy_password = Settings::get('proxy_password')) {
          if ($proxy_username) {
            $proxy[CURLOPT_PROXYUSERPW] .= $proxy_password;
          }
          else {
            $proxy[CURLOPT_PROXYUSERPW] = ':' . $proxy_password;
          }
        }
        $config['http']['curl'] = $proxy;
      }

      $embera = new \Embera\Embera($config);

      $url = ltrim($item->uri, '/');
      if (strpos($url, 'http') === FALSE && strpos($url, 'embed://') === FALSE) {
        // @todo Use https if the site is configured to use https.
        $url = 'http://' . $url;
      }

      $response = $embera->getUrlInfo($url);
      $constraint->module_handler->alter('media_entity_oembed_response', $response);
      if (empty($response) || !is_array($response)) {
        $this->context->addViolation($constraint->message);
      }
    }
  }

}
