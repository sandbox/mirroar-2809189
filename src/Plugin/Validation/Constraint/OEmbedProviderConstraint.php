<?php

/**
 * @file
 * Contains \Drupal\media_entity_oembed\Plugin\Validation\Constraint\OEmbedProviderConstraint.
 */

namespace Drupal\media_entity_oembed\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Check oembed provider type.
 *
 * @Constraint(
 *   id = "OEmbedProvider",
 *   label = @Translation("OEmbed provider type", context = "Validation"),
 * )
 */
class OEmbedProviderConstraint extends Constraint {

  public $source_field;
  public $providers;
  public $negate;
  public $module_handler;

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'The url must match one of the allowed providers.';

}
