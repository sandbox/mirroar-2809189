<?php

/**
 * @file
 * Contains \Drupal\media_entity_oembed\Plugin\MediaEntity\Type\OEmbed.
 */

namespace Drupal\media_entity_oembed\Plugin\MediaEntity\Type;

use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media_entity\MediaBundleInterface;
use Drupal\media_entity\MediaInterface;
use Drupal\media_entity\MediaTypeBase;
use Drupal\media_entity\MediaTypeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides media type plugin for Slideshows.
 *
 * @MediaType(
 *   id = "oembed",
 *   label = @Translation("OEmbed"),
 *   description = @Translation("Provides business logic and metadata for external media.")
 * )
 */
class OEmbed extends MediaTypeBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler;
   */
  protected $moduleHandler;

  /**
   * Constructs a new OEmbed instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   Module handler service.
   * @param \Drupal\Core\Config\Config $config
   *   Media entity config object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ModuleHandler $module_handler, Config $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $config);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('module_handler'),
      $container->get('config.factory')->get('media_entity.settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function providedFields() {
    // @todo Provide fields returned by Embera.
    $fields = array();

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getField(MediaInterface $media, $name) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'source_field' => NULL,
      'providers' => "YouTube\nVimeo",
      'negate' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\media_entity\MediaBundleInterface $bundle */
    $bundle = $form_state->getFormObject()->getEntity();
    $options = [];
    $allowed_field_types = ['link'];
    /** @var \Drupal\Core\Field\FieldDefinitionInterface $field */
    foreach ($this->entityFieldManager->getFieldDefinitions('media', $bundle->id()) as $field_name => $field) {
      if (in_array($field->getType(), $allowed_field_types)) {
        $storage = $field->getFieldStorageDefinition();
        if (!$storage->isBaseField()) {
          $options[$field_name] = $field->getLabel();
        }
      }
    }
    $form['source_field'] = [
      '#type' => 'select',
      '#title' => t('Field with source url'),
      '#description' => t('Field on media entity that stores external media urls.') . ' ' . t('You can create a bundle without selecting a value for this dropdown initially. This dropdown can be populated after adding fields to the bundle.'),
      '#default_value' => $this->configuration['source_field'],
      '#options' => $options,
    ];

    $form['providers'] = array(
      '#title' => t('Filter providers'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['providers'],
    );

    $form['negate'] = array(
      '#title' => t('Negate condition'),
      '#type' => 'radios',
      '#default_value' => (int) $this->configuration['negate'],
      '#options' => array(
        0 => t('Allow only listed providers'),
        1 => t('Allow all except listed providers'),
      ),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function attachConstraints(MediaInterface $media) {
    parent::attachConstraints($media);

    if (empty($this->configuration['source_field'])) {
      return;
    }

    $source_field = $this->configuration['source_field'];
    // Validate url field against allowed providers.
    $media->getTypedData()->getDataDefinition()->addConstraint('OEmbedProvider', array(
      'source_field' => $source_field,
      'providers' => array_map('trim', explode("\n", $this->configuration['providers'])),
      'negate' => $this->configuration['negate'],
      'module_handler' => $this->moduleHandler,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultThumbnail() {
    return $this->config->get('icon_base') . '/oembed.png';
  }

  /**
   * {@inheritdoc}
   */
  public function thumbnail(MediaInterface $media) {
    $source_field = $this->configuration['source_field'];

    if (!empty($source_field)) {
      // @todo Make default thumbnail destination configurable, since
      // media_entity does not let you change the thumbnail field's settings.
      $thumbnail_destination = 'public://oembed_thumbnails';
      $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('media', $media->bundle());
      if (!empty($field_definitions['thumbnail']['settings']['file_directory'])) {
        $thumbnail_destination = $field_definitions['thumbnail']['settings']['file_directory'];
      }
      file_prepare_directory($thumbnail_destination, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

      $embera = new \Embera\Embera();

      // Try and generate a preview.
      $url = $media->{$source_field}[0]->uri;

      // Remove unspecified prefixes.
      $url = ltrim($url, '/');
      if (strpos($url, 'http') === FALSE && strpos($url, 'embed://') === FALSE) {
        // @todo Use https if the site is configured to use https.
        $url = 'http://' . $url;
      }

      $response = $embera->getUrlInfo($url);
      if (!empty($response) && is_array($response)) {
        $result = end($response);
        if (!empty($result['thumbnail_url'])) {
          $thumbnail = $result['thumbnail_url'];

          // Retrieve thumbnail as an unmanaged file. The media entity module
          // will convert it to a managed file.
          $preview = system_retrieve_file($thumbnail, $thumbnail_destination, FALSE);
        }
      }

      if (!empty($preview)) {
        return $preview;
      }
    }

    return $this->getDefaultThumbnail();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultName(MediaInterface $media) {
    // The default name will be the url of the source_field, if present,
    // or the parent's defaultName implementation if it was not possible to
    // retrieve the url.
    $source_field = $this->configuration['source_field'];

    if (!empty($source_field) && ($url = $media->{$source_field}->url)) {
      return $url;
    }

    return parent::getDefaultName($media);
  }

}
