<?php

/**
 * @file
 * Contains Drupal\media_entity_oembed\Plugin\Field\FieldFormatter\OEmbedFormatter.
 */

namespace Drupal\media_entity_oembed\Plugin\Field\FieldFormatter;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'oembed' formatter.
 *
 * @FieldFormatter(
 *   id = "oembed",
 *   label = @Translation("OEmbed"),
 *   field_types = {
 *     "link"
 *   },
 * )
 */
class OEmbedFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler;
   */
  protected $moduleHandler;

  /**
   * Constructs an OEmbedFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ModuleHandler $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'providers' => "Youtube\nVimeo",
      'negate' => FALSE,
      'width' => 500,
      'height' => 300,
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['width'] = array(
      '#title' => t('Width'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('width'),
    );
    $element['height'] = array(
      '#title' => t('Height'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('height'),
    );
    $element['providers'] = array(
      '#title' => t('Filter providers'),
      '#type' => 'textarea',
      '#default_value' => $this->getSetting('providers'),
    );
    $element['negate'] = array(
      '#title' => t('Negate condition'),
      '#type' => 'radios',
      '#default_value' => (int) $this->getSetting('negate'),
      '#options' => array(
        0 => t('Allow only listed providers'),
        1 => t('Allow all except listed providers'),
      ),
      '#default_value' => $this->getSetting('negate'),
    );
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $providers = $this->getSetting('providers');
    $services = t('all');
    if (!empty($providers)) {
      $services = t('some');
    }

    $summary[] = t('@width x @height from @services services', array(
      '@width' => (int) $this->getSetting('width'),
      '@height' => (int) $this->getSetting('height'),
      '@services' => $services,
    ));

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      // @todo Add default embera config from settings.
      $config = array(
        'allow' => array(),
        'deny'  => array(),
        'params' => array(
          'width'  => $settings['width'],
          'height' => $settings['height'],
        ),
      );
      if (!empty($settings['providers'])) {
        if (!empty($settings['negate'])) {
          $config['deny'] = array_map('trim', explode("\n", $settings['providers']));
        }
        else {
          $config['allow'] = array_map('trim', explode("\n", $settings['providers']));
        }
      }

      // Use Drupal-Settings for HTTP-proxy-Server (see settings.php).
      if ($proxy_server = Settings::get('proxy_server')) {
        $proxy = array(
          CURLOPT_PROXY => $proxy_server,
          CURLOPT_PROXYPORT => Settings::get('proxy_port', 8080),
        );

        if ($proxy_username = Settings::get('proxy_username')) {
          $proxy[CURLOPT_PROXYUSERPW] = $proxy_username . ':';
        }
        if ($proxy_password = Settings::get('proxy_password')) {
          if ($proxy_username) {
            $proxy[CURLOPT_PROXYUSERPW] .= $proxy_password;
          }
          else {
            $proxy[CURLOPT_PROXYUSERPW] = ':' . $proxy_password;
          }
        }
        $config['http']['curl'] = $proxy;
      }

      $embera = new \Embera\Embera($config);

      $url = ltrim($item->uri, '/');
      if (strpos($url, 'http') === FALSE && strpos($url, 'embed://') === FALSE) {
        // @todo Use https if the site is configured to use https.
        $url = 'http://' . $url;
      }

      $response = $embera->getUrlInfo($url);
      $this->moduleHandler->alter('media_entity_oembed_response', $response);
      if (!empty($response) && is_array($response)) {
        $result = end($response);
        $elements[$delta] = array(
          '#type' => 'inline_template',
          '#template' => '{{ value|raw }}',
          '#context' => array(
            'value' => $result['html'],
          ),
        );
      }
    }

    return $elements;
  }

}
